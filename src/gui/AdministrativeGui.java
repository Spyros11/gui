package gui;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class AdministrativeGui extends JFrame {
	private JPanel panel;
	private JButton button;

	public AdministrativeGui() {
		super("Administrative");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(new Dimension(800, 400));

		setLayout(new BorderLayout());

		panel = new JPanel();

		button = new JButton("Method");

		panel.add(button);

		add(panel, BorderLayout.NORTH);

		setVisible(true);

	}
}
