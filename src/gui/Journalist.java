package gui;

public class Journalist {

	private String firstName, password;

	public Journalist(String firstName, String password) {
		this.firstName = firstName;
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
