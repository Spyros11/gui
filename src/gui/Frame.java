package gui;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Frame extends JFrame {

	private JPanel panel;
	private JLabel label, label2;
	private JButton button;
	private JTextArea textArea;
	private JTextField userText;
	private JPasswordField passText;

	private Journalist reporter;
	private Administrative admin;

	public Frame() {
		super("Welcome");

		// creating to objects to check later
		reporter = new Journalist("James", "Parker");
		admin = new Administrative("admin", "admin");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setSize(new Dimension(800, 400));

		setLayout(new BorderLayout());

		panel = new JPanel();

		// label is the text
		label = new JLabel("Username");
		userText = new JTextField(20);

		label2 = new JLabel("Password");
		passText = new JPasswordField(20);

		// press me is the text inside the button
		button = new JButton("Login");
		button.addActionListener(new ButtonListener());

		panel.add(label);
		panel.add(userText);
		panel.add(label2);
		panel.add(passText);
		panel.add(button);

		textArea = new JTextArea();

		add(panel, BorderLayout.NORTH);
		add(textArea, BorderLayout.CENTER);

		setVisible(true);
	}

	private class ButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == button) {
				// retrieve input text from user
				String name = userText.getText();
				String password = passText.getText();

				// should be checked with db names
				if (name.equals(reporter.getFirstName()) && password.equals(reporter.getPassword())) {
					textArea.append("Correct u are Journalist\n");
					new JournalistGui();
				} else if (name.equals(admin.getFirstName()) && password.equals(admin.getPassword())) {
					textArea.append("Correct u are Administrative\n");

				} else {
					textArea.append("Wrong login chief\n");

				}

				// new Reporter(name, password);
			}

		}

	}

}
